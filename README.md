想让jenkins容器可以执行宿主机上的docker命令。因为需要给予jenkins用户sudo权限，然而官方的镜像jenkins默认是没有sudo用户权限的，所以我在官方镜像的基础上新建了一个镜像，默认给jenkins用户sudo权限。

dockerfile 文件如上

运行这个镜像的容器命令：
```
docker run -d \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v $(which docker):/usr/bin/docker 
	-p 8080:8080 \
	andyzhshg/jenkins
```

第1行是将宿主机的``` /var/run/docker.sock``` 映射到容器中，这样在容器中运行的 docker 命令，就会在宿主机上来执行。

第2行是将宿主机的 docker 程序映射进容器中，这样本身没有安装 docker 的 jenkins 容器就可以执行 docker 命令了（事实上容器里是没有运行 docker 的服务的，我们只是用这个映射进容器的 docker 来作为客户端发送docker的指令到 ```/var/run/docker.sock ```而已，而 ```/var/run/docker.sock ```已经被链接到宿主机了）。